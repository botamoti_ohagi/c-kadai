#include "Enemy.h"

Enemy::Enemy()
{
	hp = 200;
	atk = 40;
	def = 20;
}
void Enemy::DispHP()
{
	std::cout << "プレイヤーHP=" << hp << "\n";
}

//攻撃
int Enemy::Attack(int i)
{
	std::cout << ("敵の攻撃\n");
	damage = atk - i / 2;
	return  damage;
}

//ダメージを受ける
void Enemy::Damage(int i)
{
	hp = hp - i;
	std::cout << "敵は" << i << "のダメージ\n";

}
