#include <iostream>
#include "Status.h"

int main()
{
	int lv;
	Status st;
	while (true) //0が入るとfalseが帰ってくるからループが終わる
	{
		std::cout << "レベルを入力⇒";
		std::cin >> lv;
		if (!st.SetLv(lv))  break;	
		st.Calc();
		std::cout << "HP=" << st.GetHp() << "\n";
		std::cout << "攻撃力=" << st.GetAtk() << "\n";
		std::cout << "防御力=" << st.GetDef() << "\n";
	}
}