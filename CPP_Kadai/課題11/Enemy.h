#include <iostream>
class Enemy
{
	int hp, atk, def;
	int damage = 0;

public:
	Enemy();
	void DispHP();
	int Attack(int i);
	void Damage(int i);
	int GetDef();
	bool IsDead();
};

