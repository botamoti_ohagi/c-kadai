#include "Player.h"

//初期化
Player::Player()
{
	hp = 300;
	atk = 50;
	def = 35;
}

//HP表示
void Player::DispHP()
{
	std::cout << "プレイヤーHP=" << hp << "\n";
}

//攻撃
int Player::Attack(int i)
{
	std::cout<<("プレイヤーの攻撃\n");
	damage= atk - i / 2;
	return  damage;
}

//ダメージを受ける
void Player::Damage(int i)
{
	hp = hp - i;
	std::cout << "プレイヤーは" << i << "のダメージ\n";
}

//防御力を取得
int Player::GetDef()
{
	return def;
}

//戦闘不能判定
bool Player::IsDead()
{
	if (hp <= 0)
		return true;

	return false;
	
}