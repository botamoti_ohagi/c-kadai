#include <iostream>
#include "Player.h"
#include "Enemy.h"

int main()
{
	Player pl;//プレイヤークラスのインスタンス
	Enemy ene;//敵クラスのインスタンス

	int damage;//攻撃した時のダメージ量

	//どちらかのHPが0以下になるまでループ
	for (int turn = 1;; turn++)
	{
		std::cout << "\n======" << turn << "ターン目======\n";

		//それぞれのHPを表示
		pl.DispHP();
		ene.DispHP();

		//主人公の攻撃
		damage = pl.Attack(ene.GetDef());
		ene.Damage(damage);
		if (ene.IsDead() == true) break;

		//敵の攻撃
		damage = ene.Attack(pl.GetDef());
		pl.Damage(damage);

		if (pl.IsDead() == true) break;
	}

	std::cout << "終了\n";
}