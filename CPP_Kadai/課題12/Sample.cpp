#include "Sample.h"
#include <iostream>

//メンバ変数に値を代入するメンバ関数
void Sample::Input() 
{
	a = 10;
	b = 3;
}
//メンバ変数同士の計算を行うメンバ関数
void Sample::Plus() 
{
	c = a + b;
}
//メンバ変数の内容を出力するメンバ関数
void Sample::Disp()
{
	std::cout << "変数Ｃの値は" << c << "\n";
}