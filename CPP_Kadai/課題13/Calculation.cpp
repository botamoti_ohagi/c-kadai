#include <iostream>
#include "Calculation.h"

void Calculation::SetA(float value)
{
	a = value;
}

void Calculation::SetB(float value)
{
	b = value;
}

void Calculation::Disp()
{
	std::cout << a << "+" << b << "=" << (a + b) << "\n";
	std::cout << a << "-" << b << "=" << (a - b) << "\n";
}
